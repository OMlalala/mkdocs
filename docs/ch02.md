## 破题

### 画流程图

190107, 19:52-20:22, .5h

先在纸上画过程图

图

## 开发

先完成最简单的流程: 输入 "/", 只打印最后一次的时间差.

检验: 能够无限次输入 "," 和 ".", 只要输入 "/", 就打印最后一次时间差, 同时退出程序.

--------------------------------------------

### 提交 issue 和 git commit

190107, 20:23-20:52, .5h

先发 issue, 再提交 git commit, 观察时间线.

尝试在 git commit message 输入多行

https://stackoverflow.com/questions/29933349/how-can-i-make-git-commit-messages-divide-into-multiple-lines

-----------------------------------------------

### 使用 list

先按着自己的思路继续嗯哼, 将时间存在 list 里.

将单次的时间, 存在 list 里 -> ```list.append()```

对历次时间求和 -> ```sum(list)```

好像已经实现了!

再看一下任务.

确实功能都实现了, 但是有一个小细节, ```记时?()``` 中间的文字增加了, 嗯哼上!

发两个 git commit: 增加 list, 修改打印的文字

## 待增补:

看到大妈对小焱的嗯哼, 发现 

```
有关输入事件响应复杂的问题, ch01 课程幻灯中有暗示:

PythoniCamp
其实, 也是怕大家乱撞, 虽然可以很快乐, 但是, 效率是下降的...
```

链接: http://io.101.camp/

看到一堆没见过的名词!

先不管, 继续按自己的思路嗯哼, 完了再增补.

## 简要回顾

- 根据流程图拆解
- 将用户输入后, 电脑执行的动作拆分 (只代表类别, 不代表顺序)
	- `记录` 时间
	- `打印` 时间
		- 字符串格式
	- `计算` 时间
	- `存储` 时间
- 先完成最小行动 -> 减少变量 (存储时间) -> 先不考虑存储, 只先增加 "/"
- 迭代 -> 增加变量 (存储时间) -> `list`
- 看大妈的课程幻灯中其他知识点, 找另一种实现方式

190107, 21:09-21:39, .5h

------------------------------------------------

# 进阶任务

拆解 ch02 总任务, 更新 issue 内容
- 基础任务
- 进阶任务

确认进阶任务的最小行动: 学习 gitlab-pages

嗯哼资料
- 官网: https://about.gitlab.com/product/pages/
- 文档: https://docs.gitlab.com/ee/user/project/pages/
- 例子: https://gitlab.com/pages

先从官网开始嗯哼

-2216 暂停, 2222 继续

先看官网 -> 了解大概步骤, 见过几个关键名词

再看 快速启动: https://docs.gitlab.com/ee/user/project/pages/#getting-started

fork pelican

190107,  22:02-22:38 .5h

--------------------------------------------------------

按着步骤继续, 

看视频 https://www.youtube.com/watch?v=TWqh9MtT4Bg, 界面不一样, 中途退出.

问题: 配好的 gitlab-pages 网页地址是什么?

看文档: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-domain

地址是: https://omlalala.gitlab.io/pelican/

另外两步
- Remove the fork relationship: setting -> general -> advanced -> remove fork relationship

A List of Static Site Generators: https://www.staticgen.com/

意外发现, gitlab 也可以集成 Gitbook
- https://gitlab.com/pages/gitbook
- https://www.kenming.idv.tw/simple-create-gitbook_at_gitlab_steps/

190107, 22:39-23:17 .5h

==============================================================

重新梳理7日晚对开发任务的拆解

- [x] gitlab pages 原理初次理解
- [x] 通过仓库实现 gitlab pages
- [ ] 通过分支实现 gitlab pages
- [ ] 使用 Gitbook 评注?

### gitlab pages 原理初次理解

官网: https://about.gitlab.com/product/pages/

目的, 简要输入初次理解

#### 什么是 gitlab pages?
- 为不同的 gitlab 账户建立网站
- 使用 静态网站生成器
- 连接你的 自定义域名 和 TLS 证明
- 部署静态网站

#### 待填坑
- 不同的 gitlab 账户区别
	- projects account
	- groups account
	- user account
- 静态网站生成器
	- jekyll
	- middleman
	- Hexo
	- ...
- 自定义域名
- TLS 证明
- gitlab 实例
- gl-pages + mkdocs: https://www.adcisolutions.com/knowledge/how-work-project-documentation
- runners
	- shared runners (使用别人建好的)
	- set my runners (自己新建)
- [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html)

通过以上初次理解, 了解相关主要概念

#### 通过仓库实现 gitlab pages

按照官网主页的步骤, 通过仓库实现 gitlab pages

quick start:

大妈在 https://gitlab.com/101camp 还 fork 了三个仓库
- mkdocs
- pelican
- plain-html

依次使用这三个仓库, 实现 gl-pages

1. 以 fork mkdocs 为例, 简述步骤

- fork 仓库
	- namespace

190108, 05:10-05:40, .5h

=============================================================

2. fork 好以后, 使用 Runners
- 定义: A 'Runner' is a process which runs a job. 执行任务的程序
- shared runners
- 新项目, 默认使用 shared runners

3. 配置你的项目(可选)
几种 pages
- [user or group pages](https://docs.gitlab.com/ee/user/project/pages/introduction.html#user-or-group-pages)
- [project pages](https://docs.gitlab.com/ee/user/project/pages/introduction.html#project-pages)

目前打开网页, 还是返回 404

图

因为第三步说的是, 配置最后的网址, 所以先跳过这步, 继续.

简要回顾:
- projects pages: https://omlalala.gitlab.io/mkdocs
- user pages: https://omlalala.gitlab.io
	- 我 fork 了多个仓库, 但是用户页面显示的是 mkdocs (如果配置的话)

4. 手动运行管道

CI / CD -> pipeline

create pipeline

5. 浏览你的网站

此时, 如果没有配置 用户页面, 即可登录 项目页面 查看网站.

https://omlalala.gitlab.io/mkdocs

验证

- settings -> pages 页面显示已成功配置
- 打开网站, 但是显示 404 ?
	- 稍等片刻, 有点慢. 参见: https://forum.gitlab.com/t/gitlab-pages-404-for-even-the-simplest-setup/5870/8
- 成功配好

3. 回到 第三步, 将 mkdocs 配置成 用户页面 试试
重命名仓库

注意修改位置: 
- 不是在 setting -> general projects
- 而是在 setting -> advanced

目前的配置 (projects account)

修改成 用户页面 (user account )

稍等片刻, 看是否登录 https://omlalala.gitlab.io 有惊喜

验证:
- settings -> pages 页面的网址已更新
- 登录用户页面, 显示 mkdocs 的内容
- 登录项目页面, 显示 pelican 的内容


190108, 05:45-06:25, .5h


=============================================================


晚上在天津, 先不嗯哼 ch02 了
- 网络不好, 连结 gitlab 非常慢...

填之前的坑: git

## 嗯哼 Git

### 缘起
对 git 有一些基础
- 之前的开智编程课有接触过
- 后来的自怼圈也都有用到

但是没有较正式嗯哼过, 之前使用的也是完成任务, 先凑或者用. 所以这次, 借着蟒营的机会, 教会自己理解 git

### 目标
输出一篇文章
- 简要介绍关于 git 最基础核心的内容, 检验自己的理解是否正确.
- 实践 了解/熟悉/掌握 一个新工具的 方法/最佳实践

### 起点
官网: https://git-scm.com/

资料结构
- 官网首页的介绍文字
- about 页面 (7个特点)
- 文档
	- 极简
		- cheat sheet (两种)
		- 教程(2部分)
		- giteveryday
		- 术语
	- 较多
		- 视频
		- 书 Pro Git
		- refernece
		- 外部链接
			- 短小/甜美
				- http://gitimmersion.com/
				- https://backlog.com/git-tutorial/
- https://github.com/git/git
	- readme 推荐了嗯哼顺序, 已经 `git` 取名由来~

### 逐一嗯哼

190108, 23:10-23:55, .5h

============================================================

### 官网首页的介绍文字

- git 是免费开源的 **分布式版本控制系统**
	- version control system (VCS)
	- distributed VS centralized
- SCM 工具 (Source Configuration Management)
	- Subversion (SVN)
	- CVS (Concurrent Versions System)
	- Perforce
	- ClearCase
- 此段感受: 各种缩写大比拼!
- 花了几分钟看了 [SVN 文档](http://svnbook.red-bean.com/en/1.7/svn-book.html#svn.intro.whatis)
	- What Is Subversion?
	- Is Subversion the Right Tool?

### About 页面 -> Git 七武器!

#### Branching and Merging
Git 终极大杀器:  branching model
- 允许多个本地分支, 彼此完全独立.
- 秒速实现分支操作(创建, 合并, 删除)

其他特性
- 待下回分解

190109, 05:50-06:20, .5h

============================================================

继续 git 的 About 页面

#### Branching and Merging
- 无束上下文切换 
- 分支的角色扮演
	- 有的仅包含 生产
	- 有的涉及合并 测试
	- 有的日常嗯哼
- 以特性为基础的工作流
	- 每一个特性开一个新分支
- 一次性实验
	- 分支的功能不合适就删除

推到远程仓库时, 可有选择推送分支.

其他工具也能实现, 但是没有 git 优雅.

#### Small and Fast

- 快
	- 几乎所有操作都在本地. 极大优势(相比较集中系统需要不时与服务器连接)
	- 站在 Linux kernel 和 C 语言的肩膀上
	- 速度与性能, 是设计 git 之初的主要目标

- 对标 集中式版本控制系统
	- 修改后是否需要立即推送到远程仓库 (集中与分布的一个区别)
	- 初次克隆操作
		- git 下载全部历史
			- 所有文件的所有历史版本
		- SVN 下载最近版本

#### Distributed

最攒劲的特性 -> 克隆整个历史, 而不是 checkout 源代码的现有 tip

- 多重备份
- 三种工作流
	- SVN style
	- Integration Manager Workflow
		-  This is the type of development model often seen with open source or GitHub repositories.
	- Dictator and Lieutenants Workflow

190109, 06:45-07:15, 新看两个特点 .5h

==============================================================


回顾进展

首先将 mkdocs 的 pages, 由 user pages -> project pages

拆解任务

现在已经实现的
- 在自己的仓库中 master 分支实现 gl-pages

计划实现
- 在自己仓库的 test 分支实现 gl-pages
- 在蟒营 playground 仓库的自己分支, 实现 gl-pages (group pages?)
- 替换 readthedocs 主题
- 增加评注模块
	- disqus?
	- utterances?

### 在自己仓库的 test 分支实现 gl-pages

### 待填坑
- utterances
	- 大妈使用 utterances 的证据: https://github.com/101camp/http/issues
	- https://lisp-journey.gitlab.io/blog/we-now-have-comments-thanks-utterances/
	- https://developer.kore.ai/docs/bots/what-are-kore-bots/addingbots/integrating-with-gitlab/

190109 20:00-20:30, .5h

==============================================================

190109 20:32-21:10, .5h

- 在本地配置
	- 安装 mkdocs: `brew install mkdocs`
		- 网速不给力啊, 太慢了...要安装三个( gdbm, python, mkdocs)
	`➜  mkdocs git:(master) mkdocs --version
mkdocs, version 1.0.4 from /usr/local/Cellar/mkdocs/1.0.4_1/libexec/lib/python3.7/site-packages/mkdocs (Python 3.7)
➜  mkdocs git:(master)`
	- 新建一个分支 gl-test, 完全复制 master 分支的文件: `git branch -c master gl-test`
	- 切换到测试分支 gl-test: `git checkout gl-test`
	- 在 docs 里新建一个文件
	- 在 mkdocs.yml 里配置导航
	- 在 .gitlab-ci.yml 里切换分支

=============================================================

190109, 21:20-21:50, .5h


### 如何在同一个仓库的不同分支, 实现不同的 pages?

- 猜想: 所有分支都有一个 .gitlab-ci.yml, 每个的内容里对应各自分支.
- 验证: 
	- 不同分支, 都有一个 .gitlab-ci.yml, 但是发布版本, 会覆盖.
	- 但是不同的分支只对应一个 page 网址呀?

=============================================================

190109, 22:25-22:55 , .5h

遇到问题, 不同分支只能对应一个网址, 和之前想的不同分支对应不同网址不同. 

重新整理思路.

重新看 ch02 拓展任务文字

发现下面的提示
`参考资料,扩展阅读...`: https://mister-gold.pro/posts/en/deploy-pelican-on-gitlab-pages/

看完了, 可是没提到关于多个网址的事情呀!

忽然想到:

能否: 在 py 仓库的私人分支, 提交变更时, 同时提交到自己的私人仓库!
私人仓库配置了 gitlab-pages!

===========================================================

190109, 23:25-23:55, .5h

又想了一下, 这样可能比较麻烦, 如果只用私人分支, 就能实现, 就没必要再关联个人仓库..

感觉自己对几种 page 还理解不到位, 再看一下官方文档

- https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-domain

如何在 group 里, 创建自己的 user pages?

还是没有解决...

忽然想到

参考大妈的博客架构. 仓库嵌套!

私人分支嗯哼代码, 然后通过 pelican, 将 output 设置成私人仓库, 然后输出 gitlab-pages

问题: 
- 私人仓库输出的 gl-pages, 其他蟒营的学员能否看到? 
- 非蟒营的人能否开到? 是否涉及课程内容公开? 可参考之前开智的课程, 私人 fork 课程私人仓库后, 建立的 Gitbook 是否公开?

=============================================================

回顾思路

- 先在 playground 仓库, 私人分支, 配置 pelican, output -> 私人仓库
-> 自己的 user pages
- 若成功, 则在 py 仓库部署

- zsh 报错
```
dyld: Library not loaded: /usr/local/opt/gdbm/lib/libgdbm.4.dylib
  Referenced from: /usr/local/bin/zsh
  Reason: image not found
zsh: abort
``` 
解决: https://github.com/robbyrussell/oh-my-zsh/issues/7033

升级更新: `brew upgrade zsh`

- 待填坑:
issue 正文带目录: https://github.com/thlorenz/doctoc

网速慢呀...

小焱在 slack 问配置服务器事宜.

190110, 23:00-23:30, .5h

=============================================================

在 playground 仓库, omlalala 分支, 克隆 gitlab-pages pelican 仓库

结果克隆后, 并没有在当前文件夹, 而是 pelican 文件夹

```
➜  playground git:(OMlalala) git clone git@gitlab.com:pages/pelican.git
Cloning into 'pelican'...
remote: Enumerating objects: 49, done.
remote: Counting objects: 100% (49/49), done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 49 (delta 21), reused 0 (delta 0)
Receiving objects: 100% (49/49), 9.40 KiB | 4.70 MiB/s, done.
Resolving deltas: 100% (21/21), done.
➜  playground git:(OMlalala) ✗ ls
README.md pelican
```

想将文件克隆在当前文件夹, 查:

如何在 pelican 仓库中, 再嵌套一个仓库.

可以克隆私人仓库, 然后取名为 public.

问题:

重新 git clone playground 仓库.

结果

```
➜  playground git:(master) git checkout OMlalala
Branch 'OMlalala' set up to track remote branch 'OMlalala' from 'origin'.
Switched to a new branch 'OMlalala'
```
```
➜  playground git:(OMlalala) git st
On branch OMlalala
Your branch is up to date with 'origin/OMlalala'.

nothing to commit, working tree clean
```

```
➜  playground git:(ch01) git checkout origin/OMlalala
Note: checking out 'origin/OMlalala'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>

HEAD is now at 5b81bce Initial Commit
➜  playground git:(5b81bce) git st
HEAD detached at origin/OMlalala
nothing to commit, working tree clean
```

190110, 23:33-00:15, .5h

=============================================================

问题: 想把 pelican 里的文件, 全部拷贝到 playground 仓库的 OMlalal 分支里

测试 1: 直接克隆
```
  playground git:(OMlalala) git remote -v
origin	git@gitlab.com:101camp/playground.git (fetch)
origin	git@gitlab.com:101camp/playground.git (push)
➜  playground git:(OMlalala) cd ..
➜  101camp git clone git@gitlab.com:pages/pelican.git playground
fatal: destination path 'playground' already exists and is not an empty directory.
```
因为分支里现在不是空文件夹, 所以没法直接克隆.

测试 2: 先添加远程仓库, 再 pull
```
➜  playground git:(OMlalala) git remote add origin git@gitlab.com:pages/pelican.git
fatal: remote origin already exists.
```
结果: `origin` 已经被用了, 换一个名字
```
➜  playground git:(OMlalala) git remote add pelican git@gitlab.com:pages/pelican.git
➜  playground git:(OMlalala) git remote -v
origin	git@gitlab.com:101camp/playground.git (fetch)
origin	git@gitlab.com:101camp/playground.git (push)
pelican	git@gitlab.com:pages/pelican.git (fetch)
pelican	git@gitlab.com:pages/pelican.git (push)
```

使用 `git pl`, 预期两个仓库的内容, 都能更新, 结果只更新了 playgound 的仓库:

```
➜  playground git:(OMlalala) git pl
remote: Enumerating objects: 12, done.
remote: Counting objects: 100% (12/12), done.
remote: Compressing objects: 100% (12/12), done.
remote: Total 12 (delta 3), reused 0 (delta 0)
Unpacking objects: 100% (12/12), done.
From gitlab.com:101camp/playground
   f7e3c3a..1b89ceb  ch02        -> origin/ch02
   58731be..40f8db9  chenhao0327 -> origin/chenhao0327
Already up to date.
```

使用 `git fetch` 只更新了 playground 的内容, 并没有更新 pelican 的内容:
```
➜  playground git:(OMlalala) git fetch pelican
warning: no common commits
remote: Enumerating objects: 49, done.
remote: Counting objects: 100% (49/49), done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 49 (delta 21), reused 0 (delta 0)
Unpacking objects: 100% (49/49), done.
From gitlab.com:pages/pelican
 * [new branch]      master          -> pelican/master
 * [new branch]      revert-29124a11 -> pelican/revert-29124a11
➜  playground git:(OMlalala) ls
README.md
```

明确拉的仓库和分支也不行:
```
➜  playground git:(OMlalala) git pull pelican master
From gitlab.com:pages/pelican
 * branch            master     -> FETCH_HEAD
fatal: refusing to merge unrelated histories
```

查询解决:
https://stackoverflow.com/questions/37937984/git-refusing-to-merge-unrelated-histories-on-rebase


再次实验, 文件是拷贝了, 但需要处理 `README.md` 冲突. 此时的 `README.md` 文件是新拷贝过来的, 会覆盖原来的.
```
➜  playground git:(OMlalala) git pull pelican master --allow-unrelated-histories
From gitlab.com:pages/pelican
 * branch            master     -> FETCH_HEAD
Auto-merging README.md
CONFLICT (add/add): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
➜  playground git:(OMlalala) ✗ ls
Makefile          content           fabfile.py        publishconf.py
README.md         develop_server.sh pelicanconf.py    requirements.txt
```

所需要的 pelican 文件已拷贝至 OMlalala 分支, 可以删除 remote 了.
```
➜  playground git:(OMlalala) git remote -v
origin	git@gitlab.com:101camp/playground.git (fetch)
origin	git@gitlab.com:101camp/playground.git (push)
pelican	git@gitlab.com:pages/pelican.git (fetch)
pelican	git@gitlab.com:pages/pelican.git (push)
➜  playground git:(OMlalala) git remote rm pelican
➜  playground git:(OMlalala) git remote -v
origin	git@gitlab.com:101camp/playground.git (fetch)
origin	git@gitlab.com:101camp/playground.git (push)
```

回顾进展: 现在已将 pelican 文件拷贝至 playground 仓库的 OMlalala 分支, 下面要实现的是:
- [ ] 在此分支里嵌套仓库, 将 /public 文件夹添加至远程私人仓库
- [ ] 在远程私人仓库, 配置 gl-pages 的 user pages

- markdown 新技能 get: 添加分割线, 语法:
```
---
```

### 待填坑
- `git fetch` 和 `git pull` 区别?

190112, 13:50-14:20, .5h

---

### [x] 在此分支里嵌套仓库, 将 /public 文件夹添加至远程私人仓库

- [x] 检验: 之前 fork 的两个仓库 (mkdocs 和 pelican) 是否 project pages, 确保不能使用 user pages.(因为要留给蟒营)

OK, 两个都是 project pages:
- https://omlalala.gitlab.io/mkdocs/
- https://omlalala.gitlab.io/pelican/

- [x] 新建一个仓库, 用于蟒营的 user pages.
先试一下 public, 配置 gl-pages, 然后再试 private, 看能否只让蟒营的用户看到笔记.

- [x] 将 /public 文件夹添加至远程私人仓库
```
➜  playground git:(OMlalala) git clone git@gitlab.com:OMlalala/omlalala.gitlab.io.git public
Cloning into 'public'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
➜  playground git:(OMlalala) ls
Makefile          content           fabfile.py        public            requirements.txt
README.md         develop_server.sh pelicanconf.py    publishconf.py
```

### [ ] 在远程私人仓库, 配置 gl-pages 的 user pages

按照文档步骤: https://gitlab.com/pages/pelican#building-locally
, 发现我没有安装 pelican
```
➜  playground git:(OMlalala) make html
pelican /Users/omlalala/repo.omlalala.io/101camp/playground/content -o /Users/omlalala/repo.omlalala.io/101camp/playground/public -s /Users/omlalala/repo.omlalala.io/101camp/playground/pelicanconf.py
make: pelican: No such file or directory
make: *** [html] Error 1
```
先按照 `requirements.txt` 的要求安装.

```
pip install -r requirements.txt
```
安装成功后, 再次实验
```
➜  playground git:(OMlalala) ✗ make html
pelican /Users/omlalala/repo.omlalala.io/101camp/playground/content -o /Users/omlalala/repo.omlalala.io/101camp/playground/public -s /Users/omlalala/repo.omlalala.io/101camp/playground/pelicanconf.py
Done: Processed 1 article, 0 drafts, 0 pages, 0 hidden pages and 0 draft pages in 0.19 seconds.
```

结果输入 `make serve` 又有问题
```
➜  playground git:(OMlalala) ✗ make serve
cd /Users/omlalala/repo.omlalala.io/101camp/playground/public && python2 -m pelican.server
pyenv: python2: command not found

The `python2' command exists in these Python versions:
  2.7.14
  2.7.15

make: *** [serve] Error 127
```

变更 `Makefile` 的 python 版本, 由`python2`改为`python`:
```
PY?=python
PELICAN?=pelican
PELICANOPTS=
```
再次输入`make serve`:
```
➜  playground git:(OMlalala) ✗ make serve
cd /Users/omlalala/repo.omlalala.io/101camp/playground/public && python -m pelican.server
/Users/omlalala/.pyenv/versions/3.7.0/lib/python3.7/runpy.py:125: RuntimeWarning: 'pelican.server' found in sys.modules after import of package 'pelican', but prior to execution of 'pelican.server'; this may result in unpredictable behaviour
  warn(RuntimeWarning(msg))
WARNING: 'python -m pelican.server' is deprecated.
  | The Pelican development server should be run via 'pelican --listen' or 'pelican -l'.
  | This can be combined with regeneration as 'pelican -lr'.
  | Rerun 'pelican-quickstart' to get new Makefile and tasks.py files.
-> Serving at port 8000, server .
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET /theme/css/main.css HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET /theme/css/reset.css HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET /theme/css/pygment.css HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET /theme/css/typogrify.css HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET /theme/css/fonts.css HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 14:56:56] "GET /theme/fonts/Yanone_Kaffeesatz_400.woff HTTP/1.1" 200 -
WARNING: Unable to find `/favicon.ico` or variations:
  | /favicon.ico.html
  | /favicon.ico/index.html
  | /favicon.ico/
  | /favicon.ico
^C-> Shutting down server.
```

190112, 14:25-15:00, .5h

---

现状: 已经在 `public/` 文件夹里生成文件了, 为何进入 user pages 还未显示对应的 pelican 文件?

猜测: 因为我只是在本地生成了, 还没有推送到远程仓库?

验证: 将变更推送到远程仓库.

关掉原 mkdocs 的 pipeline

新建 `.gitlab-ci.yml`, 按照 pelican 的对应文件创建内容:https://gitlab.com/pages/pelican/blob/master/.gitlab-ci.yml

问题: 打开用户页面, 显示的还是 mkdocs 页面内容, 非 pelican 内容!

190112, 15:05-15:35, .5h

---

再看一下 课程给的提示: https://mister-gold.pro/posts/en/deploy-pelican-on-gitlab-pages/

在 public 里本地看网页, 是能看到 pelican 的:

```
➜  public git:(master) ls
README.md                    authors.html                 index.html                   tags.html
archives.html                categories.html              pelican-on-gitlab-pages.html theme
author                       category                     tag
➜  public git:(master) python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
127.0.0.1 - - [12/Jan/2019 16:20:31] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [12/Jan/2019 16:20:32] "GET /theme/css/main.css HTTP/1.1" 304 -
127.0.0.1 - - [12/Jan/2019 16:20:32] "GET /theme/css/reset.css HTTP/1.1" 304 -
127.0.0.1 - - [12/Jan/2019 16:20:32] "GET /theme/css/pygment.css HTTP/1.1" 304 -
127.0.0.1 - - [12/Jan/2019 16:20:32] "GET /theme/css/typogrify.css HTTP/1.1" 304 -
127.0.0.1 - - [12/Jan/2019 16:20:32] "GET /theme/css/fonts.css HTTP/1.1" 304 -
127.0.0.1 - - [12/Jan/2019 16:20:32] "GET /theme/fonts/Yanone_Kaffeesatz_400.woff HTTP/1.1" 304 -
127.0.0.1 - - [12/Jan/2019 16:20:35] "GET /category/gitlab.html HTTP/1.1" 200 -
^C
Keyboard interrupt received, exiting.
```

但是在网页端打开, 还是显示 mkdocs! 

问题: 如果将 public 里的文件夹就看成纯 html 文件, 如何让 gl-pages 识别并渲染?

忽然想到:

大妈提供的另一个思路: https://gitlab.com/101camp/plain-html

190112, 16:00-16:30, .5h

---

整理思路
- fork plain-html 仓库, 更名为 omlalala.gitlab.io
```
➜  playground git:(OMlalala) ✗ git clone git@gitlab.com:OMlalala/plain-html.git omlalala.gitlab.io
Cloning into 'omlalala.gitlab.io'...
remote: Enumerating objects: 49, done.
remote: Counting objects: 100% (49/49), done.
remote: Compressing objects: 100% (36/36), done.
remote: Total 49 (delta 10), reused 49 (delta 10)
Receiving objects: 100% (49/49), 7.80 KiB | 3.90 MiB/s, done.
Resolving deltas: 100% (10/10), done.
➜  playground git:(OMlalala) ✗ ls
Makefile           __pycache__        develop_server.sh  omlalala.gitlab.io publishconf.py
README.md          content            fabfile.py         pelicanconf.py     requirements.txt
➜  playground git:(OMlalala) ✗ cd omlalala.gitlab.io
➜  omlalala.gitlab.io git:(master) ls
README.md public
➜  omlalala.gitlab.io git:(master) git remote -v
origin	git@gitlab.com:OMlalala/plain-html.git (fetch)
origin	git@gitlab.com:OMlalala/plain-html.git (push)
```

- 取关 plain-html 仓库, 关联 omlalala.gitlab.io 仓库
```
➜  omlalala.gitlab.io git:(master) git remote rm origin
➜  omlalala.gitlab.io git:(master) git remote -v
➜  omlalala.gitlab.io git:(master) ✗ git remote add origin git@gitlab.com:OMlalala/omlalala.gitlab.io.git
➜  omlalala.gitlab.io git:(master) ✗ git remote -v
origin	git@gitlab.com:OMlalala/omlalala.gitlab.io.git (fetch)
origin	git@gitlab.com:OMlalala/omlalala.gitlab.io.git (push)
➜  omlalala.gitlab.io git:(master) ✗ cd ..
➜  playground git:(OMlalala) ✗ git remote -v
origin	git@gitlab.com:101camp/playground.git (fetch)
origin	git@gitlab.com:101camp/playground.git (push)
```

- 修改两个文件, 将 pelican 的输出文件夹, 改为 `omlalala.gitlab.io/public`

- 生成网页文件, 发现地址变更成功, 新的网页文件已生成
```
➜  playground git:(OMlalala) ✗ make html
pelican /Users/omlalala/repo.omlalala.io/101camp/playground/content -o /Users/omlalala/repo.omlalala.io/101camp/playground/omlalala.gitlab.io/public -s /Users/omlalala/repo.omlalala.io/101camp/playground/pelicanconf.py
Done: Processed 1 article, 0 drafts, 0 pages, 0 hidden pages and 0 draft pages in 0.14 seconds.
➜  playground git:(OMlalala) ✗ cd omlalala.gitlab.io
➜  omlalala.gitlab.io git:(master) ✗ ls
README.md public
➜  omlalala.gitlab.io git:(master) ✗ cd public
➜  public git:(master) ✗ ls
archives.html                categories.html              pelican-on-gitlab-pages.html theme
author                       category                     tag
authors.html                 index.html                   tags.html
```

- 将 omlalala.gitlab.io 的变更推送到远程仓库

- 在网页端配置 gl-pages

成功! 

现在 用户页面 已经成为 pelican 的文件内容了! https://omlalala.gitlab.io/

190112, 16:40-17:10, .5h

---

190113, 15:55-16:25, .5h

回顾进展:

现在已经实现了用 pelican 搭建学习笔记, 现在打算使用 mkdocs 搭建, 考虑原因
- mkdocs 比 pelican 更容易搭建, 更节省蟒营学员的精力.
- 用途: mkdocs 针对文档, 更适合学习笔记的使用场景.

所以, 接下来的行动为, 使用 mkdocs 搭建学习笔记.

预期步骤
- 先在自己的仓库实验
	- 将 doc 里的文档, 替换成自己的学习笔记
		- 如何增加文档, 与组织文档目录结构
	- 将主题替换成 readthedocs
- 在 playground 仓库实验
- 在 py 仓库实验

### - [ ] 将 doc 里的文档, 替换成自己的学习笔记

